import 'dart:async';
import 'dart:io';

import 'package:flutter/services.dart';

class FlutterSocialShare {
  static const MethodChannel _channel =
      const MethodChannel('flutter_social_share');

  static final String whatsappAndroidAppId = 'com.whatsapp';
  static final String whatsappIOSAppId = 'whatsapp://';
  static final String whatsappAppStoreAppId = '310633997';

  static Future<String> get platformVersion async {
    final String version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }

  static void shareWhatsappMessageToNumber(Map<String, String> props) async {
    bool isAvailable = false;
    if (Platform.isAndroid) {
      isAvailable = await checkAppAvailability(whatsappAndroidAppId);
    } else {
      isAvailable = await checkAppAvailability(whatsappIOSAppId);
    }

    if (isAvailable) {
      await _channel.invokeMethod('sendSingleWhatsappMessage', props);
    } else {
      await _channel.invokeMethod('redirectToStore', {
        'android_id': whatsappAndroidAppId,
        'ios_id': whatsappAppStoreAppId
      });
    }
  }

  static Future<bool> checkAppAvailability(String uri) async {
    Map<String, dynamic> args = <String, dynamic>{};
    args.putIfAbsent('uri', () => uri);
    final bool isAvailable =
        await _channel.invokeMethod("checkAvailability", args);
    return isAvailable;
  }
}
