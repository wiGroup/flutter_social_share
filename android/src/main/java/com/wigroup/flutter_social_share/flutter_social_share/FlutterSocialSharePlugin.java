package com.wigroup.flutter_social_share.flutter_social_share;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;

import androidx.annotation.NonNull;

import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.embedding.engine.plugins.activity.ActivityAware;
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;
import io.flutter.plugin.common.PluginRegistry.Registrar;

/**
 * FlutterSocialSharePlugin
 */
public class FlutterSocialSharePlugin implements FlutterPlugin, MethodCallHandler, ActivityAware {

    private static Activity activity;

    /// The MethodChannel that will the communication between Flutter and native Android
    ///
    /// This local reference serves to register the plugin with the Flutter Engine and unregister it
    /// when the Flutter Engine is detached from the Activity
    private MethodChannel channel;

    @Override
    public void onAttachedToEngine(@NonNull FlutterPluginBinding flutterPluginBinding) {
        channel = new MethodChannel(flutterPluginBinding.getFlutterEngine().getDartExecutor(), "flutter_social_share");
        channel.setMethodCallHandler(this);
    }

    // This static function is optional and equivalent to onAttachedToEngine. It supports the old
    // pre-Flutter-1.12 Android projects. You are encouraged to continue supporting
    // plugin registration via this function while apps migrate to use the new Android APIs
    // post-flutter-1.12 via https://flutter.dev/go/android-project-migration.
    //
    // It is encouraged to share logic between onAttachedToEngine and registerWith to keep
    // them functionally equivalent. Only one of onAttachedToEngine or registerWith will be called
    // depending on the user's project. onAttachedToEngine or registerWith must both be defined
    // in the same class.
    public static void registerWith(Registrar registrar) {
        final MethodChannel channel = new MethodChannel(registrar.messenger(), "flutter_social_share");
        channel.setMethodCallHandler(new FlutterSocialSharePlugin());
        FlutterSocialSharePlugin.activity = registrar.activity();
    }

    @Override
    public void onMethodCall(@NonNull MethodCall call, @NonNull Result result) {
        if (call.method.equals("checkAvailability")) {
            String uriSchema = call.argument("uri").toString();
            this.checkAvailability(uriSchema, result);
        } else if (call.method.equals("sendSingleWhatsappMessage")) {
            this.sendSingleMessage(call, result);
        } else if (call.method.equals("redirectToStore")) {
            openAppStore(call, result);
        } else {
            result.notImplemented();
        }
    }

    //Check App Availability
    private void checkAvailability(String uri, Result result) {
        PackageInfo info = getAppPackageInfo(uri);
        if (info != null) {
            result.success(true);
        } else {
            result.success(false);
        }
    }

    private PackageInfo getAppPackageInfo(String uri) {
        final PackageManager pm =  activity.getPackageManager();

        try {
            return pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
        } catch (PackageManager.NameNotFoundException e) {
        }

        return null;
    }
    //End Check App Availability

    //Send Single Message
    private void sendSingleMessage(MethodCall call, Result result) {
        final PackageManager pm =  activity.getPackageManager();
        Intent i = new Intent(Intent.ACTION_VIEW);
        try {
            String mobileNo = call.argument("mobileNo");
            String message = call.argument("message");
            String url = "https://wa.me/" + mobileNo.trim() + "?text=" + message.trim();
            i.setPackage("com.whatsapp");
            i.setData(Uri.parse(url));
            if (i.resolveActivity(pm) != null) {
                activity.startActivity(i);
            }
        } catch (Exception e) {
            e.printStackTrace();
            result.notImplemented();
        }
    }
    //End Send Single Message

    //Open app store
    private void openAppStore(MethodCall call, Result result) {
        String appId = call.argument("android_id");
        String appPackageName;

        if (appId != null) {
            appPackageName = appId;
        } else {
            appPackageName = activity.getPackageName();
        }

        Intent marketIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName));
        marketIntent.addFlags(
                Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_NEW_DOCUMENT | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        activity.startActivity(marketIntent);

        result.success(null);
    }
    //End open app store

    @Override
    public void onDetachedFromEngine(@NonNull FlutterPluginBinding binding) {
        channel.setMethodCallHandler(null);
    }

    @Override
    public void onAttachedToActivity(@NonNull ActivityPluginBinding binding) {
        if (FlutterSocialSharePlugin.activity == null) {
            FlutterSocialSharePlugin.activity = binding.getActivity();
        }
    }

    @Override
    public void onDetachedFromActivityForConfigChanges() {

    }

    @Override
    public void onReattachedToActivityForConfigChanges(@NonNull ActivityPluginBinding binding) {

    }

    @Override
    public void onDetachedFromActivity() {

    }
}
