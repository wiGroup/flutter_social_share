import Flutter
import UIKit

public class SwiftFlutterSocialSharePlugin: NSObject, FlutterPlugin {
    public static func register(with registrar: FlutterPluginRegistrar) {
        let channel = FlutterMethodChannel(name: "flutter_social_share", binaryMessenger: registrar.messenger())
        let instance = SwiftFlutterSocialSharePlugin()
        registrar.addMethodCallDelegate(instance, channel: channel)
    }
    
    public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
        if (call.method == "checkAvailability") {
            if let args = call.arguments as? Dictionary<String, Any>,
                let uriSchema = args["uri"] as? String
            {
                let isAppAvailable = checkAvailability(uri: uriSchema)
                result(isAppAvailable)
            }
            
        } else if (call.method == "sendSingleWhatsappMessage") {
            DispatchQueue.main.async{
                guard let args = call.arguments else {
                    return
                }
                if let myArgs = args as? [String: Any],
                    let mobileNo = myArgs["mobileNo"] as? String,
                    let message = myArgs["message"] as? String {
                    //result("Params received on iOS = \(someInfo1), \(someInfo2)")

                    let urlString = "whatsapp://send?phone=\(mobileNo.trimmingCharacters(in: .whitespaces))&text=\(message.trimmingCharacters(in: .whitespaces))"
                    let urlStringEncoded = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)

                    let whatsAppUrl = NSURL(string: urlStringEncoded!)
                    if UIApplication.shared.canOpenURL(whatsAppUrl! as URL) {
                        UIApplication.shared.openURL(whatsAppUrl! as URL)
                    }
                }
            }
        } else if(call.method == "redirectToStore") {
            if let args = call.arguments as? Dictionary<String, Any>,
                let appId = args["ios_id"] as? String
            {
                if let url = URL(string: "itms-apps://itunes.apple.com/app/" + appId),
                    UIApplication.shared.canOpenURL(url)
                {
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(url, options: [:], completionHandler: nil)
                    } else {
                        UIApplication.shared.openURL(url)
                    }
                }
            }
        }else{
            result(FlutterMethodNotImplemented)
        }
    }
    
    public func checkAvailability (uri: String) -> Bool {
        let url = URL(string: uri)
        return UIApplication.shared.canOpenURL(url!)
    }
}
