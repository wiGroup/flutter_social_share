# flutter_social_share

Flutter Social Share Plugin

Please note that phone number must startwith country postfix
i.e Mauritian phone number should begin with 230
    SA phone numbers should begin with 27

//Combination of

  flutter_open_whatsapp: ^0.1.2
  flutter_appavailability: ^0.0.21
  store_redirect: ^1.0.2


## Getting Started

This project is a starting point for a Flutter
[plug-in package](https://flutter.dev/developing-packages/),
a specialized package that includes platform-specific implementation code for
Android and/or iOS.

For help getting started with Flutter, view our 
[online documentation](https://flutter.dev/docs), which offers tutorials, 
samples, guidance on mobile development, and a full API reference.
