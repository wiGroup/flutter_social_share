import 'package:flutter/material.dart';
import 'dart:async';
import 'package:flutter_social_share/flutter_social_share.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  static const Map<String, String> _singleMessageProps = {
    "mobileNo": "23057471234",
    "message": "Test message",
  };

  @override
  void initState() {
    super.initState();
  }

  Future _shareWhatsappMessageToNumber(
      Map<String, String> singleMessageProps) async {
    FlutterSocialShare.shareWhatsappMessageToNumber(singleMessageProps);
  }

  _button(String title, Function onPressed) {
    return RaisedButton(
      color: Color.fromARGB(255, 134, 250, 194),
      textColor: Color.fromARGB(255, 33, 28, 92),
      child: Padding(
          padding: EdgeInsets.all(16),
          child: Text(
            title,
            style: TextStyle(fontSize: 16),
          )),
      onPressed: onPressed,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(18.0),
          side: BorderSide(color: Color.fromARGB(255, 134, 250, 194))),
    );
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Color.fromARGB(255, 246, 246, 246),
        appBar: AppBar(
          title: const Text('Social Share| Demo app'),
          backgroundColor: Color.fromARGB(255, 55, 63, 67),
        ),
        body: Center(
          child: ListView(
              padding: EdgeInsets.all(20.0),
              shrinkWrap: true,
              children: <Widget>[
                _button("Whatsapp",
                    () => _shareWhatsappMessageToNumber(_singleMessageProps)),
                SizedBox(height: 20),
              ]),
        ),
        bottomSheet: Container(
          padding: EdgeInsets.all(10.0),
          color: Colors.white,
          width: double.infinity,
          child: Text("Developed with ♥ by Leonardo Lerasse"),
        ),
      ),
    );
  }
}
